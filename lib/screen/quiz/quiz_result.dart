import 'package:countup/countup.dart';
import 'package:flash_cards/screen/quiz/wrong_list.dart';
import 'package:flash_cards/store/quiz_store.dart';
import 'package:flash_cards/store/word_store.dart';
import 'package:flash_cards/util/WordType.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class QuizResult extends StatelessWidget {
  final QuizStore quizStore = Get.find();
  final WordStore wordStore = Get.find();
  final Function onRestart;

  QuizResult({Key key, @required this.onRestart}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Observer(
        builder: (_) => Container(
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: Container(
                decoration: BoxDecoration(
                    color: Color(0xff5B57A2),
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30))),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CircularPercentIndicator(
                        animation: true,
                        radius: (MediaQuery.of(context).size.height / 1.5 - 50) / 2,
                        lineWidth: 3.0,
                        progressColor: Colors.white,
                        backgroundColor: Colors.white54,
                        percent: quizStore.percent,
                        center: Countup(
                            begin: 0,
                            end: quizStore.percent * 100 ?? 0,
                            prefix: "% ",
                            duration: Duration(milliseconds: 500),
                            separator: ',',
                            style: TextStyle(color: Colors.white),
                            textScaleFactor: 1.2),
                      ),
                      SizedBox(height: 10),
                      Container(
                        margin: EdgeInsets.only(bottom: 5),
                        decoration:
                            BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(20))),
                        width: 200,
                        height: 60,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text("Doğru",
                                      style: TextStyle(color: Color(0xff2B894A), fontWeight: FontWeight.w500),
                                      textScaleFactor: 1.1),
                                  Text(quizStore.correctCount.toString(),
                                      style: TextStyle(fontWeight: FontWeight.w600), textScaleFactor: 1.2)
                                ],
                              ),
                            ),
                            VerticalDivider(),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text("Yanlış",
                                      style: TextStyle(color: Color(0xffC51A0D), fontWeight: FontWeight.w500),
                                      textScaleFactor: 1.1),
                                  Text(
                                    quizStore.falseCount.toString(),
                                    style: TextStyle(fontWeight: FontWeight.w600),
                                    textScaleFactor: 1.2,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )),
              Expanded(
                  child: IntrinsicWidth(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: buildButtons(context)),
              ))
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> buildButtons(context) {
    return [
      FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          height: 50,
          color: Color(0xff5B57A2),
          child: Row(
            children: [
              Icon(MdiIcons.fileSearchOutline, color: Colors.white),
              SizedBox(width: 10),
              Text("Yanlışlarımı Göster", style: TextStyle(color: Colors.white)),
            ],
          ),
          onPressed: () {
            Get.to(WrongList(answers: quizStore.userAnswers.where((element) => !element.selectedAnswer.correct).toList()));
          }),
      SizedBox(height: 10),
      FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          height: 50,
          color: Color(0xff5B57A2),
          child: Row(
            children: [
              Icon(MdiIcons.schoolOutline, color: Colors.white),
              SizedBox(width: 10),
              Text("Yeni Kelimeler Öğren", style: TextStyle(color: Colors.white)),
            ],
          ),
          onPressed: () {
            wordStore.fetchWords();
            wordStore.setPage(0);
            wordStore.setType(WordType.BROWSE);
            Get.offAndToNamed("/browse");
          }),
      SizedBox(height: 10),
      FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          height: 50,
          color: Color(0xff5B57A2),
          child: Row(
            children: [
              Icon(MdiIcons.progressQuestion, color: Colors.white),
              SizedBox(width: 10),
              Text("Tekrar Sınav Ol", style: TextStyle(color: Colors.white)),
            ],
          ),
          onPressed: onRestart),
    ];
  }
}
