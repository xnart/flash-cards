import 'package:flash_cards/model/question_model.dart';
import 'package:flash_cards/screen/quiz/quiz_type.dart';
import 'package:flash_cards/store/setting_store.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WrongList extends StatelessWidget {
  final List<QuestionModel> answers;
  final SettingStore settingStore = Get.find();

  WrongList({Key key, this.answers}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Yanlışlarım"),
      ),
      backgroundColor: Colors.white,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: settingStore.quizType == QuizType.MEANING ? buildTable() : buildListView(),
      ),
    );
  }

  Widget buildTable() {
    return DataTable(
        columns: const <DataColumn>[
          DataColumn(
            label: Text('Kelime'),
          ),
          DataColumn(
            label: Text('Doğru Cevap'),
          ),
          DataColumn(
            label: Text('Cevabınız'),
          ),
        ],
        rows: answers
            .map((e) => DataRow(
                  cells: [
                    DataCell(Text(e.word.word)),
                    DataCell(Text(settingStore.quizType == QuizType.MEANING ? e.word.meaning : e.word.description)),
                    DataCell(Text(settingStore.quizType == QuizType.MEANING
                        ? e.selectedAnswer.word.meaning
                        : e.selectedAnswer.word.description)),
                  ],
                ))
            .toList());
  }

  Widget buildListView() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView(
          children: answers
              .map((e) => Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(e.word.word, textScaleFactor: 1.2, style: TextStyle(fontWeight: FontWeight.w600, color: Colors.pinkAccent)),
                          SizedBox(height: 10),
                          Text(e.word.description, style: TextStyle(color: Colors.grey.shade700)),
                          SizedBox(height: 15),
                          Text("Cevabınız:"),
                          SizedBox(height: 10),
                          Text(e.selectedAnswer.word.description, style: TextStyle(color: Colors.grey.shade700)),
                        ],
                      ),
                ),
              ))
              .toList()),
    );
  }
}
