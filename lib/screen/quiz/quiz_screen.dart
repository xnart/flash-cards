import 'package:flash_cards/component/my_observer.dart';
import 'package:flash_cards/model/answer_model.dart';
import 'package:flash_cards/screen/quiz/animated_button.dart';
import 'package:flash_cards/screen/quiz/quiz_result.dart';
import 'package:flash_cards/screen/quiz/quiz_type.dart';
import 'package:flash_cards/store/ad_store.dart';
import 'package:flash_cards/store/home_store.dart';
import 'package:flash_cards/store/quiz_store.dart';
import 'package:flash_cards/store/setting_store.dart';
import 'package:flash_cards/store/word_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get/get.dart';
import 'package:sticky_headers/sticky_headers.dart';

class QuizScreen extends StatefulWidget {
  @override
  _QuizScreenState createState() => _QuizScreenState();
}

class _QuizScreenState extends State<QuizScreen> {
  final HomeStore homeStore = Get.find();

  final QuizStore quizStore = Get.find();

  final WordStore wordStore = Get.find();

  final SettingStore settingStore = Get.find();
  final AdStore adStore = Get.find();

  final PageController pageController = PageController();

  bool askType = true;

  void _askQuestionType() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        askType = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (askType) {
      return Scaffold(
          backgroundColor: Color(0xff5B57A2).withOpacity(0.5),
          body: Center(
            child: Container(
              width: 350,
              margin: EdgeInsets.all(10),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(32.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Sınav Türünü Seçiniz", textScaleFactor: 1.2),
                      SizedBox(height: 15),
                      Container(
                        width: 200,
                        height: 50,
                        child: RaisedButton(
                            onPressed: () {
                              settingStore.setQuizType(QuizType.MEANING);
                              setState(() {
                                askType = false;
                              });
                            },
                            child: Text("Kelimeden Anlam", textScaleFactor: 1.1, style: TextStyle(color: Colors.white)),
                            color: Colors.pink.shade800),
                      ),
                      SizedBox(height: 15),
                      Container(
                        width: 200,
                        height: 50,
                        child: RaisedButton(
                            onPressed: () {
                              settingStore.setQuizType(QuizType.DESCRIPTION);
                              setState(() {
                                askType = false;
                              });
                            },
                            child:
                                Text("Kelimeden Açıklama", textScaleFactor: 1.1, style: TextStyle(color: Colors.white)),
                            color: Colors.green.shade800),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ));
    }
    return WillPopScope(
      onWillPop: () async {
        _onBackPressed(context);
        return false;
      },
      child: Scaffold(
        backgroundColor: Color(0xff5B57A2),
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: () => {_onBackPressed(context)},
          ),
          title: Center(child: Observer(builder: (_) => Text(quizStore.title))),
          backgroundColor: Color(0xff5B57A2),
          elevation: 0,
        ),
        body: MyObserver(
          future: () => homeStore.wordsFuture,
          onRetry: () => homeStore.fetchWords(),
          builder: (_) {
            return PageView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemCount: quizStore.questions.length + 1,
                controller: pageController,
                itemBuilder: (_, i) {
                  if (i == quizStore.questions.length) {
                    adStore.showInterstitialAd();
                    return QuizResult(
                      onRestart: () {
                        setState(() {
                          askType = true;
                        });
                        _askQuestionType();
                        wordStore.fetchWords();
                        quizStore.setPage(0);
                        quizStore.init();
                        pageController.jumpToPage(0);
                      },
                    );
                  }
                  return Container(
                    margin: EdgeInsets.all(7),
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height,
                    child: Card(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.all(32.0),
                          child: Observer(
                            builder: (_) {
                              var currentQuestion = quizStore.currentQuestion;
                              var answerButtons = buildAnswers();
                              return StickyHeader(
                                header: Container(
                                  color: Colors.white,
                                  child: Text(currentQuestion.word.word,
                                      textScaleFactor: 1.8, style: TextStyle(fontWeight: FontWeight.bold)),
                                ),
                                content: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    SizedBox(height: 20),
                                    GridView.count(
                                      physics: ScrollPhysics(),
                                      shrinkWrap: true,
                                      padding: EdgeInsets.all(5),
                                      childAspectRatio: settingStore.quizType == QuizType.MEANING ? 1.4 : 5,
                                      crossAxisCount: settingStore.quizType == QuizType.MEANING ? 2 : 1,
                                      crossAxisSpacing: 20,
                                      mainAxisSpacing: 20,
                                      children: answerButtons,
                                    )
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  );
                });
          },
        ),
      ),
    );
  }

  void _onAnswered(AnswerModel answerModel) async {
    quizStore.setAnswered(answerModel);
    Future.delayed(Duration(milliseconds: 500)).then((value) {
      quizStore.nextQuestion();
      pageController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  List<Widget> buildAnswers() {
    var answers = quizStore.currentQuestion.answers;

    var list = answers
        .map((a) =>
            AnimatedButton(currentQuestion: quizStore.currentQuestion, answerModel: a, onPressed: () => _onAnswered(a)))
        .toList();

    return list;
  }

  void _onBackPressed(BuildContext context) {
    if (quizStore.showFinishPage) {
      Get.back();
      return;
    }
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Emin misiniz?'),
        content: new Text('Sınavdan çıkmak istediğinize emin misiniz ? Cevaplarınız kaydedilmeyecek'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text("İPTAL"),
          ),
          SizedBox(height: 16),
          TextButton(
            onPressed: () {
              Get.close(2);
            },
            child: Text("EVET"),
          ),
        ],
      ),
    );
  }
}
