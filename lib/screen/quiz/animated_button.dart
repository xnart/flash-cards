import 'package:animated_widgets/animated_widgets.dart';
import 'package:flash_cards/model/answer_model.dart';
import 'package:flash_cards/model/question_model.dart';
import 'package:flash_cards/screen/quiz/quiz_type.dart';
import 'package:flash_cards/store/quiz_store.dart';
import 'package:flash_cards/store/setting_store.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AnimatedButton extends StatelessWidget {
  final QuizStore quizStore = Get.find();
  final SettingStore settingStore = Get.find();
  final QuestionModel currentQuestion;
  final AnswerModel answerModel;
  final Function onPressed;

  AnimatedButton({Key key, this.answerModel, this.onPressed, this.currentQuestion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String text;
    if (settingStore.quizType == QuizType.MEANING) {
      text = answerModel.word.meaning;
    } else {
      text = answerModel.word.description;
    }
    return ShakeAnimatedWidget(
      shakeAngle: Rotation.deg(z: 3),
      duration: Duration(milliseconds: 500),
      enabled: currentQuestion.answered &&
          currentQuestion.selectedAnswer == answerModel &&
          !currentQuestion.selectedAnswer.correct,
      child: FlatButton(
        padding: EdgeInsets.all(5),
        color: _pickColor(),
        shape: RoundedRectangleBorder(side: BorderSide(width: 0.3), borderRadius: BorderRadius.circular(10)),
        onPressed: onPressed,
        child: Text(text,
            style: TextStyle(color: currentQuestion.selectedAnswer == answerModel ? Colors.white : Colors.black)),
      ),
    );
  }

  Color _pickColor() {
    if (currentQuestion.answered && currentQuestion.selectedAnswer == answerModel) {
      if (answerModel.correct) {
        return Color(0xff12D6C1);
      }
      return Color(0xffB54663);
    }
    return Colors.white;
  }
}
