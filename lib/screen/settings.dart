import 'package:flash_cards/store/setting_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get/get.dart';

class SettingsScreen extends StatelessWidget {
  final SettingStore store = Get.find();

  SettingsScreen() {
    store.fetchSettings();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Ayarlar"), backgroundColor: Color(0xff272c49)),
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Observer(
            builder: (_) => ListView(
              children: [
                Text("Öğrenme Ayarları", style: TextStyle(color: Colors.pink)),
                ListTile(
                  title: Text("Oturum başına öğrenilen kelime"),
                  trailing: DropdownButton(
                    value: store.wordPerSession,
                    items: [
                      DropdownMenuItem(child: Text("5"), value: 5),
                      DropdownMenuItem(child: Text("10"), value: 10)
                    ],
                    onChanged: store.setWordPerSession,
                  ),
                ),
                Divider(),
                Text("Sınav Ayarları", style: TextStyle(color: Colors.pink)),
                ListTile(
                  title: Text("Oturum başına sorulan soru"),
                  trailing: DropdownButton(
                    value: store.questionPerSession,
                    items: [
                      DropdownMenuItem(child: Text("5"), value: 5),
                      DropdownMenuItem(child: Text("10"), value: 10)
                    ],
                    onChanged: store.setQuestionPerSession,
                  ),
                ),
                ListTile(
                  title: Text("Her soru için şık sayısı"),
                  trailing: DropdownButton(
                    value: store.answerPerQuestion,
                    items: [DropdownMenuItem(child: Text("6"), value: 6), DropdownMenuItem(child: Text("8"), value: 8)],
                    onChanged: store.setAnswerPerQuestion,
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
