import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flash_cards/main.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  List<Slide> slides = new List();

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "YDS KELİME KARTLARI",
        description: "YDS'de sık soruların kelimeleri öğren, daha sonra tekrar etmek için favorilere ekle",
        pathImage: "images/icon1.png",
        backgroundColor: Color(0xff5B57A2),
      ),
    );
    slides.add(
      new Slide(
        title: "KENDİNİ DENE",
        description: "Sınavlara katılarak kendini dene, eksiklerini gör",
        pathImage: "images/exam.png",
        backgroundColor: Color(0xff5B57A2).withOpacity(0.8),
      ),
    );
  }

  void onDonePress() {
    firestore.collection("users").doc(auth.currentUser.uid).set({"showIntro": false}, SetOptions(merge: true));
    Get.offAllNamed("/");
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      nameSkipBtn: "ATLA",
      nameNextBtn: "DEVAM",
      namePrevBtn: "GERİ",
      nameDoneBtn: "BİTİR",
      slides: this.slides,
      onDonePress: this.onDonePress,
    );
  }
}
