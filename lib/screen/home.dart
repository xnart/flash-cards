import 'package:connectivity/connectivity.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:countup/countup.dart';
import 'package:flash_cards/component/gradient_button.dart';
import 'package:flash_cards/store/connectivity_store.dart';
import 'package:flash_cards/store/home_store.dart';
import 'package:flash_cards/store/word_store.dart';
import 'package:flash_cards/util/WordType.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:mobx/mobx.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../main.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final HomeStore homeStore = Get.find();
  final WordStore wordStore = Get.find();
  final ConnectivityStore connectivityStore = Get.find();

  var disposer;

  @override
  void initState() {
    super.initState();
    disposer = reaction((_) => connectivityStore.connectivityStream.value, (result) async {
      if (result == ConnectivityResult.none) {
        await firestore.disableNetwork();
        print("network disabled");
      } else {
        await firestore.enableNetwork();
        print("network enabled");
      }
    });
    homeStore.fetchWords();
  }

  @override
  void dispose() {
    dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: ConvexAppBar(
          onTap: (i) {
            if (i == 2) {
              Get.toNamed("/settings");
            }
          },
          backgroundColor: Color(0xff373d5f),
          style: TabStyle.fixedCircle,
          items: [
            TabItem(icon: Icon(Icons.hourglass_empty, size: 0), title: ''),
            TabItem(icon: Icons.home, title: 'Home'),
            TabItem(icon: Icons.settings, title: ''),
          ]),
      backgroundColor: Color(0xff272c49),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Observer(
            builder: (_) => Expanded(
              flex: 3,
              child: Stack(
                children: [
                  Positioned.fill(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
                        color: Colors.white,
                      ),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          height: 70,
                          child: Row(
                            children: [
                              buildNavButton("browse".tr, homeStore.browseCount, Icon(MdiIcons.bulletinBoard),
                                  () async {
                                wordStore.setType(WordType.BROWSE);
                                return await Get.toNamed("/browse");
                              }),
                              buildNavButton("learned".tr, homeStore.learnedCount, Icon(MdiIcons.lightbulbOutline),
                                  () async {
                                wordStore.setType(WordType.LEARNED);
                                return await Get.toNamed("/browse");
                              }),
                              buildNavButton("my_favs".tr, homeStore.favCount, Icon(MdiIcons.heartOutline), () async {
                                wordStore.setType(WordType.FAV);
                                return await Get.toNamed("/browse");
                              }),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned.fill(
                    top: 0,
                    bottom: 80,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
                        gradient: LinearGradient(colors: [Color(0xffff6d65), Color(0xffff3966)]),
                      ),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularPercentIndicator(
                              animation: true,
                              radius: (MediaQuery.of(context).size.height / 1.5 - 100) / 3,
                              lineWidth: 3.0,
                              percent: homeStore.percent,
                              center: Countup(
                                  begin: 0,
                                  end: homeStore.percent * 100 ?? 0,
                                  prefix: "% ",
                                  duration: Duration(milliseconds: 500),
                                  separator: ',',
                                  textScaleFactor: 1.2,
                                  style: TextStyle(color: Colors.white)),
                              progressColor: Colors.white,
                              backgroundColor: Colors.white54,
                            ),
                            SizedBox(height: 5),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Öğrendiğim Kelimeler",
                                    textScaleFactor: 1.3,
                                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500)),
                                SizedBox(height: 5),
                                Container(
                                  width: 150,
                                  child: LinearPercentIndicator(
                                      animation: true,
                                      progressColor: Colors.white,
                                      backgroundColor: Colors.white54,
                                      lineHeight: 3,
                                      percent: homeStore.percent),
                                ),
                                SizedBox(height: 5),
                                Countup(
                                    begin: 0,
                                    end: homeStore.wordsFuture?.value?.learned?.length?.toDouble() ?? 0,
                                    duration: Duration(milliseconds: 500),
                                    separator: ',',
                                    textScaleFactor: 1.2,
                                    style: TextStyle(color: Colors.white))
                              ],
                            ),
                          ]),
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: FractionallySizedBox(
              widthFactor: 0.6,
              heightFactor: 0.3,
              child: GradientButton(
                height: 50,
                onPressed: () => Get.toNamed("/quiz"),
                colors: [Color(0xffff3966), Color(0xffff6d65)],
                text: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.class_, color: Colors.white),
                    Text(
                      'Sınav Ol',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 17, fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildNavButton(String text, num value, Icon icon, Function onTap) {
    return Expanded(
      child: Material(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            borderRadius: BorderRadius.circular(100),
            onTap: () {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                onTap();
              });
            },
            child: Column(
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  icon,
                  Countup(
                    begin: 0,
                    end: value.toDouble(),
                    duration: Duration(milliseconds: 500),
                    separator: ',',
                    style: TextStyle(
                      color: Colors.pink,
                      fontSize: 17,
                    ),
                  )
                ]),
                Expanded(child: Text(text.toUpperCase()))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
