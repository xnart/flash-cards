import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../main.dart';

class ErrorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text("Üzgünüz", textScaleFactor: 1.3, style: TextStyle(fontWeight: FontWeight.w600)),
                SizedBox(height: 10),
                Text("İlk kullanımda internet bağlantısı gerekmektedir", textScaleFactor: 1.2),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.bottomRight,
                  child: RaisedButton(
                    color: Colors.pink.shade700,
                    onPressed: () async {
                      try {
                        await init();
                        String route = await determineRoute();
                        Get.offAllNamed(route);
                      } catch (e) {}
                    },
                    child: Text("Tekrar Dene", style: TextStyle(color: Colors.white)),
                  ),
                )
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
