import 'package:extended_image/extended_image.dart';
import 'package:flash_cards/component/my_observer.dart';
import 'package:flash_cards/model/word_model.dart';
import 'package:flash_cards/store/ad_store.dart';
import 'package:flash_cards/store/home_store.dart';
import 'package:flash_cards/store/word_store.dart';
import 'package:flash_cards/util/WordType.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sticky_headers/sticky_headers.dart';

class BrowseScreen extends StatefulWidget {
  @override
  _BrowseScreenState createState() => _BrowseScreenState();
}

class _BrowseScreenState extends State<BrowseScreen> {
  final WordStore wordStore = Get.find();
  final HomeStore homeStore = Get.find();
  final AdStore adStore = Get.find();
  bool reloadImage = false;

  final PageController pageController = PageController();

  @override
  void initState() {
    _init();
    super.initState();
  }

  void _init() {
    wordStore.fetchWords();
    wordStore.setPage(0);
  }

  Future<void> _onBack() async {
    await homeStore.fetchWords();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          await _onBack();
          return true;
        },
        child: Scaffold(
            appBar: AppBar(
              title: Observer(builder: (_) {
                if (wordStore.showFinishPage) {
                  return SizedBox.shrink();
                }
                return Center(child: Text("${wordStore.currentPage + 1}/${wordStore.currentWords?.length ?? 1}"));
              }),
              leading: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () async {
                    await _onBack();
                    Get.back();
                  }),
            ),
            bottomNavigationBar: buildBottomBar(),
            body: MyObserver(
              onRetry: _init,
              future: () => wordStore.wordsFuture,
              builder: (_) {
                var words = wordStore.currentWords;
                return PageView.builder(
                  controller: pageController,
                  itemCount: words.length + 1,
                  onPageChanged: wordStore.setPage,
                  itemBuilder: (context, index) {
                    if (wordStore.currentWords.length == 0 && wordStore.type == WordType.BROWSE) {
                      return buildNoContentPage();
                    }
                    if (index == wordStore.currentWords.length) {
                      adStore.showInterstitialAd();
                      return buildFinishPage(context);
                    }
                    final WordModel wordModel = words[index];
                    return Container(
                      margin: EdgeInsets.all(7),
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height,
                      child: FlipCard(
                        flipOnTouch: true,
                        back: Card(
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(32.0),
                              child: Text(wordModel.meaning, textScaleFactor: 1.2),
                            ),
                          ),
                        ),
                        front: Card(
                          child: SingleChildScrollView(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 15.0),
                              child: StickyHeader(
                                header: Container(
                                  color: Colors.white,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(wordModel.word,
                                          textScaleFactor: 1.8, style: TextStyle(fontWeight: FontWeight.bold)),
                                      Container(
                                        padding: EdgeInsets.all(8),
                                        decoration: BoxDecoration(border: Border.all(width: 0.9)),
                                        child: Text(wordModel.wordType, textScaleFactor: 1.1),
                                      )
                                    ],
                                  ),
                                ),
                                content: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    SizedBox(height: 10),
                                    Text(wordModel.description,
                                        textScaleFactor: 1.2, style: TextStyle(color: Colors.grey.shade600)),
                                    SizedBox(height: 20),
                                    SizedBox(height: 320, child: buildImage(wordModel.picUrl)),
                                    SizedBox(height: 20),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          "Örnek",
                                          textScaleFactor: 1.2,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.grey.shade600,
                                              decoration: TextDecoration.underline),
                                        ),
                                        Text(wordModel.example,
                                            textScaleFactor: 1.1, style: TextStyle(color: Colors.grey.shade600))
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
            )));
  }

  Widget buildImage(String url) {
    return ExtendedImage.network(
      url,
      cache: true,
      loadStateChanged: (ExtendedImageState state) {
        switch (state.extendedImageLoadState) {
          case LoadState.loading:
            return Center(child: CircularProgressIndicator());
          case LoadState.completed:
            return null;
          case LoadState.failed:
            return GestureDetector(
              behavior: HitTestBehavior.translucent,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.error, size: 35, color: Colors.pink.shade700),
                  Text(
                    "Resim yüklenemedi",
                    textScaleFactor: 1.2,
                  ),
                  Text(
                    "Tekrar denemek için dokun",
                    textScaleFactor: 1.2,
                  )
                ],
              ),
              onTap: () {
                state.reLoadImage();
              },
            );
          default:
            return Container();
        }
      },
    );
  }

  Widget buildBottomBar() {
    return MyObserver(
      future: () => wordStore.wordsFuture,
      onRetry: _init,
      builder: (_) {
        if (wordStore.showFinishPage) {
          return SizedBox.shrink();
        }
        return Container(
          margin: EdgeInsets.only(bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              FlatButton(
                  onPressed: () {
                    wordStore.toggleLearned();
                    pageController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.elasticOut);
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(wordStore.currentWord.learned ? Icons.lightbulb : Icons.lightbulb_outline,
                          color: Colors.yellowAccent.withOpacity(0.5)),
                      Text(
                        "Öğrendim",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  )),
              FlatButton(
                  onPressed: () {
                    wordStore.toggleHide();
                    pageController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.elasticOut);
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(wordStore.currentWord.hide ? MdiIcons.eye : MdiIcons.eyeOff,
                          color: Colors.white.withOpacity(0.5)),
                      Text(
                        wordStore.currentWord.hide ? "Göster" : "Gizle",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  )),
              FlatButton(
                  onPressed: () {
                    wordStore.toggleFav();
                    pageController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.elasticOut);
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(wordStore.currentWord.favorited ? MdiIcons.heart : MdiIcons.heartOutline,
                          color: Colors.red.withOpacity(0.5)),
                      Text(
                        "Favori",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ))
            ],
          ),
        );
      },
    );
  }

  Widget buildFinishPage(context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          ListTile(
              leading: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(MdiIcons.shuffleVariant, color: Colors.blue),
                ],
              ),
              title: Text("Yeni kelimeler öğren"),
              onTap: () {
                _init();
                wordStore.setType(WordType.BROWSE);
              }),
          Divider(),
          ListTile(
              leading: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(MdiIcons.crosshairsQuestion, color: Colors.red),
                ],
              ),
              title: Text("Sınav ol"),
              onTap: () => Get.offAndToNamed("/quiz")),
          ListTile(
            title: Icon(Icons.home_outlined, color: Colors.deepPurple, size: 30),
            onTap: () async {
              await _onBack();
              Get.back();
            },
          )
        ]),
      ),
    );
  }

  Widget buildNoContentPage() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Icon(Icons.check, color: Colors.green, size: 100),
          Text("Öğreneceğiniz kelime kalmamış", textScaleFactor: 1.2),
          SizedBox(height: 20),
          IconButton(
              icon: Icon(Icons.home_outlined, size: 30),
              onPressed: () async {
                await _onBack();
                Get.back();
              })
        ]),
      ),
    );
  }
}
