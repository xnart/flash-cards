import 'package:get/get.dart';

class AppTranslations extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'tr_TR': {
          'learned': 'Öğrendiklerim',
          'my_favs': 'Favorilerim',
          'browse': 'Gözden Geçir',
        },
        'en_US': {
          'hello': 'Hello World',
        }
      };
}
