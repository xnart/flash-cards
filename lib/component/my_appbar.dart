import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double height;
  final String title;
  final List<Widget> actions;

  const MyAppBar({Key key, this.height = 70, @required this.title, this.actions}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 1.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
        margin: EdgeInsets.fromLTRB(10, 10, 10, 5),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: AppBar(
              title: Text(title),
              backgroundColor: Colors.indigo.shade400,
              actions: this.actions,
            )),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
