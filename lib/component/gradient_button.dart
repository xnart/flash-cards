import 'package:flutter/material.dart';

class GradientButton extends StatelessWidget {
  final double height;
  final List<Color> colors;
  final Widget text;
  final Function onPressed;

  const GradientButton({Key key, this.height, @required this.colors, @required this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      child: RaisedButton(
        onPressed: this.onPressed,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        padding: EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: this.colors,
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
              borderRadius: BorderRadius.circular(30.0)),
          child: Container(
            constraints: BoxConstraints(minHeight: this.height),
            alignment: Alignment.center,
            child: text,
          ),
        ),
      ),
    );
  }
}
