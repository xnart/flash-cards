import 'package:flutter/material.dart';
import 'package:get/get.dart';

void showError() {
  Get.snackbar(
    "Hata",
    "Lütfen hataları düzeltin",
    colorText: Colors.white,
    backgroundColor: Colors.red,
    icon: Icon(
      Icons.error,
      color: Colors.white,
    ),
    borderRadius: 0,
    margin: EdgeInsets.zero,
    shouldIconPulse: true,
    snackPosition: SnackPosition.BOTTOM,
  );
}

void showSuccess() {
  Get.snackbar(
    "Başarılı",
    "İşlem tamamlandı",
    colorText: Colors.white,
    backgroundColor: Colors.green,
    icon: Icon(
      Icons.check,
      color: Colors.white,
    ),
    borderRadius: 0,
    margin: EdgeInsets.zero,
    shouldIconPulse: true,
    snackPosition: SnackPosition.BOTTOM,
  );
}

