import 'package:flutter/material.dart';

class EmptyWarning extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Hata")),
        body: Center(
            child: Text(
          "Burada pek bir şey yok",
          textScaleFactor: 1.2,
          style: TextStyle(color: Colors.white),
        )));
  }
}
