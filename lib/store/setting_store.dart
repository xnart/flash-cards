import 'package:flash_cards/model/setting_model.dart';
import 'package:flash_cards/repository/setting_repository.dart';
import 'package:flash_cards/screen/quiz/quiz_type.dart';
import 'package:get/get.dart';
import 'package:mobx/mobx.dart';

part 'setting_store.g.dart';

class SettingStore = _SettingStore with _$SettingStore;

abstract class _SettingStore with Store {
  SettingRepository settingRepository = Get.find();

  @observable
  int wordPerSession = 10;

  @observable
  int questionPerSession = 10;

  @observable
  int answerPerQuestion = 6;

  @observable
  QuizType quizType = QuizType.MEANING;

  @observable
  ObservableFuture<SettingModel> settingsFuture = ObservableFuture.value(null);

  @action
  ObservableFuture<SettingModel> fetchSettings() =>
      settingsFuture = ObservableFuture(settingRepository.fetchSettings());

  _SettingStore() {
    fetchSettings();
    reaction((_) => settingsFuture.status, (f) {
      if (f == FutureStatus.fulfilled) {
        setFromModel(settingsFuture.value);
      }
    });
  }

  @action
  void setFromModel(SettingModel model) {
    wordPerSession = model.wordPerSession;
    questionPerSession = model.questionPerSession;
    answerPerQuestion = model.answerPerQuestion;
    quizType = model.quizType;
  }

  @action
  void setQuizType(QuizType value) {
    quizType = value;
  }

  @action
  void setWordPerSession(int value) {
    wordPerSession = value;
    _saveSettings();
  }

  @action
  void setQuestionPerSession(int value) {
    questionPerSession = value;
    _saveSettings();
  }

  @action
  void setAnswerPerQuestion(int value) {
    answerPerQuestion = value;
    _saveSettings();
  }

  void _saveSettings() {
    SettingModel settingModel = SettingModel(
        wordPerSession: wordPerSession,
        questionPerSession: questionPerSession,
        answerPerQuestion: answerPerQuestion,
        quizType: quizType);

    settingRepository.saveSettings(settingModel);
  }
}
