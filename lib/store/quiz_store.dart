import 'dart:math';

import 'package:flash_cards/model/answer_model.dart';
import 'package:flash_cards/model/question_model.dart';
import 'package:flash_cards/model/word_model.dart';
import 'package:get/get.dart';
import 'package:mobx/mobx.dart';

import 'home_store.dart';
import 'setting_store.dart';

part 'quiz_store.g.dart';

class QuizStore = _QuizStore with _$QuizStore;

abstract class _QuizStore with Store {
  final HomeStore homeStore = Get.find();
  final SettingStore settingStore = Get.find();

  @observable
  int currentPage = 0;

  @observable
  ObservableList<QuestionModel> userAnswers = ObservableList.of([]);

  @computed
  int get correctCount => userAnswers.where((element) => element.selectedAnswer.correct).toList().length;

  @computed
  int get falseCount => userAnswers.where((element) => !element.selectedAnswer.correct).toList().length;

  @computed
  bool get showFinishPage => currentPage == questions.length;

  @observable
  QuestionModel currentQuestion = QuestionModel(answered: false, answers: []);

  @observable
  ObservableList<QuestionModel> questions = ObservableList.of([]);

  @computed
  String get title {
    if (showFinishPage) {
      return "";
    }
    return "${currentPage + 1} / ${questions.length}";
  }

  @computed
  double get percent {
    if (correctCount == 0 && falseCount == 0) {
      return 0;
    }
    return correctCount / (correctCount + falseCount);
  }

  _QuizStore(){
    init();
  }

  @action
  void init() {
    List<WordModel> all = homeStore.wordsFuture.value?.all ?? [];
    all.shuffle();
    List<QuestionModel> questions = [];
    int maxIndex = all.length - settingStore.questionPerSession;
    int startIndex = 0;
    int endIndex;
    if (maxIndex > 0) {
      startIndex = Random().nextInt(maxIndex);
      endIndex = startIndex + settingStore.questionPerSession;
    } else if (maxIndex <= 0) {
      endIndex = all.length;
    }
    all.sublist(startIndex, endIndex)
      .forEach((w) {
        var questionModel = QuestionModel();
        questionModel.word = w;
        var allWrongAnswers = all.where((element) => element.meaning != w.meaning).toList();
        int maxIndex;
        int startIndex = 0;
        int endIndex;
        if(allWrongAnswers.length <= settingStore.answerPerQuestion){
          maxIndex = allWrongAnswers.length;
          endIndex = allWrongAnswers.length;
        }else{
          maxIndex = allWrongAnswers.length - settingStore.answerPerQuestion;
          startIndex = Random().nextInt(maxIndex);
          endIndex = startIndex + settingStore.answerPerQuestion - 1;
        }
        var answers = allWrongAnswers
            .sublist(startIndex, endIndex)
            .map((e) => AnswerModel(word: e, correct: false))
            .toList();
        answers.add(AnswerModel(word: w, correct: true));
        answers.shuffle();
        questionModel.answers = answers;
        questions.add(questionModel);
      });
    this.questions = ObservableList.of(questions);
    if(questions.length > 0){
      setCurrentQuestion(questions[0]);
    }
  }

  @action
  void setPage(int val) {
    currentPage = val;
  }

  @action
  QuestionModel setCurrentQuestion(QuestionModel q) {
    currentQuestion =
        QuestionModel(answered: q.answered, answers: q.answers, word: q.word, selectedAnswer: q.selectedAnswer);

    return currentQuestion;
  }

  @action
  void setAnswered(AnswerModel answer) {
    currentQuestion.answered = !currentQuestion.answered;
    currentQuestion.selectedAnswer = answer;
    setCurrentQuestion(currentQuestion);
    userAnswers.add(QuestionModel(
        answered: true, answers: currentQuestion.answers, word: currentQuestion.word, selectedAnswer: answer));
  }

  @action
  void clearAnswer() {
    currentQuestion.answered = false;
    currentQuestion.selectedAnswer = null;
    setCurrentQuestion(currentQuestion);
  }

  @action
  void nextQuestion() {
    clearAnswer();
    if (currentPage + 1 <= questions.length) {
      currentPage++;
      if (!showFinishPage) {
        setCurrentQuestion(questions[currentPage]);
      }
    }
  }
}
