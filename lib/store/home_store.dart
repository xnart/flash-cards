import 'package:flash_cards/model/word_wrapper.dart';
import 'package:flash_cards/repository/word_repository.dart';
import 'package:get/get.dart';
import 'package:mobx/mobx.dart';

part 'home_store.g.dart';

class HomeStore = _HomeStore with _$HomeStore;

abstract class _HomeStore with Store {
  final WordRepository wordRepository = Get.find();

  @observable
  ObservableFuture<WordWrapper> wordsFuture =
      ObservableFuture.value(WordWrapper(all: [], hides: [], learned: [], favs: []));

  @action
  fetchWords() {
    wordsFuture = ObservableFuture(wordRepository.fetchWords());
  }

  @computed
  int get allCount => wordsFuture.value?.all?.length ?? 0;

  @computed
  int get learnedCount => wordsFuture.value?.learned?.length ?? 0;

  @computed
  int get favCount => wordsFuture.value?.favs?.length ?? 0;

  @computed
  int get browseCount => wordsFuture.value?.browsable?.length ?? 0;

  @computed
  double get percent {
    if (learnedCount == 0 && allCount == 0) {
      return 0;
    }
    return learnedCount / allCount;
  }
}
