import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flash_cards/model/word_model.dart';
import 'package:flash_cards/model/word_wrapper.dart';
import 'package:flash_cards/repository/word_repository.dart';
import 'package:flash_cards/util/WordType.dart';
import 'package:get/get.dart';
import 'package:mobx/mobx.dart';

import '../main.dart';
import 'setting_store.dart';

part 'word_store.g.dart';

class WordStore = _WordStore with _$WordStore;

abstract class _WordStore with Store {
  final WordRepository wordRepository = Get.find();
  final SettingStore settingStore = Get.find();


  @observable
  int currentPage = 0;

  @observable
  WordType type = WordType.ALL;

  @observable
  ObservableFuture<WordWrapper> wordsFuture = ObservableFuture.value(WordWrapper());

  @action
  fetchWords() {
    wordsFuture = ObservableFuture(wordRepository.fetchWords());
  }

  @computed
  WordModel get currentWord {
    if (currentPage < currentWords.length) {
      return currentWords[currentPage];
    }
    return currentWords[currentPage - 1];
  }

  @computed
  bool get showFinishPage {
    return currentPage == currentWords.length;
  }

  @computed
  List<WordModel> get currentWords {
    switch (type) {
      case WordType.ALL:
        return wordsFuture.value?.all ?? List.empty();
      case WordType.BROWSE:
        var browsable = wordsFuture.value?.browsable ?? List.empty();
        if (browsable.isEmpty) {
          return List.empty();
        }
        browsable.shuffle();
        int maxIndex = browsable.length - settingStore.wordPerSession;
        int startIndex = 0;
        int endIndex;
        if (maxIndex > 0) {
          startIndex = Random().nextInt(maxIndex);
          endIndex = startIndex + settingStore.wordPerSession;
        } else if (maxIndex <= 0) {
          endIndex = browsable.length;
        }

        var sublist = browsable.sublist(startIndex, endIndex);
        return sublist;
      case WordType.FAV:
        return wordsFuture.value?.favs ?? List.empty();
      case WordType.LEARNED:
        return wordsFuture.value?.learned ?? List.empty();
      case WordType.HIDE:
        return wordsFuture.value?.hides ?? List.empty();
      default:
        return List.empty();
    }
  }

  @action
  void setPage(int i) {
    currentPage = i;
  }

  @action
  void setType(WordType wordType) {
    this.type = wordType;
  }

  @action
  void toggleFav({bool hide = false}) {
    if (currentWord.favorited || hide) {
      firestore
          .collection("users")
          .doc(FirebaseAuth.instance.currentUser.uid)
          .collection("favs")
          .doc(currentWord.id)
          .delete();
      currentWord.favorited = false;
    } else {
      firestore
          .collection("users")
          .doc(FirebaseAuth.instance.currentUser.uid)
          .collection("favs")
          .doc(currentWord.id)
          .set({});
      currentWord.favorited = true;
    }
  }

  @action
  void toggleHide() {
    if (currentWord.hide) {
      firestore
          .collection("users")
          .doc(FirebaseAuth.instance.currentUser.uid)
          .collection("hides")
          .doc(currentWord.id)
          .delete();
      currentWord.hide = false;
    } else {
      firestore
          .collection("users")
          .doc(FirebaseAuth.instance.currentUser.uid)
          .collection("hides")
          .doc(currentWord.id)
          .set({});
      toggleFav(hide: true);
      toggleLearned(hide: true);
      currentWord.hide = true;
    }
  }

  @action
  void toggleLearned({hide = false}) {
    if (currentWord.learned || hide) {
      firestore
          .collection("users")
          .doc(FirebaseAuth.instance.currentUser.uid)
          .collection("learned")
          .doc(currentWord.id)
          .delete();
      currentWord.learned = false;
    } else {
      firestore
          .collection("users")
          .doc(FirebaseAuth.instance.currentUser.uid)
          .collection("learned")
          .doc(currentWord.id)
          .set({});
      currentWord.learned = true;
    }
  }
}
