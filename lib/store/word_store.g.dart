// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'word_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$WordStore on _WordStore, Store {
  Computed<WordModel> _$currentWordComputed;

  @override
  WordModel get currentWord =>
      (_$currentWordComputed ??= Computed<WordModel>(() => super.currentWord,
              name: '_WordStore.currentWord'))
          .value;
  Computed<bool> _$showFinishPageComputed;

  @override
  bool get showFinishPage =>
      (_$showFinishPageComputed ??= Computed<bool>(() => super.showFinishPage,
              name: '_WordStore.showFinishPage'))
          .value;
  Computed<List<WordModel>> _$currentWordsComputed;

  @override
  List<WordModel> get currentWords => (_$currentWordsComputed ??=
          Computed<List<WordModel>>(() => super.currentWords,
              name: '_WordStore.currentWords'))
      .value;

  final _$currentPageAtom = Atom(name: '_WordStore.currentPage');

  @override
  int get currentPage {
    _$currentPageAtom.reportRead();
    return super.currentPage;
  }

  @override
  set currentPage(int value) {
    _$currentPageAtom.reportWrite(value, super.currentPage, () {
      super.currentPage = value;
    });
  }

  final _$typeAtom = Atom(name: '_WordStore.type');

  @override
  WordType get type {
    _$typeAtom.reportRead();
    return super.type;
  }

  @override
  set type(WordType value) {
    _$typeAtom.reportWrite(value, super.type, () {
      super.type = value;
    });
  }

  final _$wordsFutureAtom = Atom(name: '_WordStore.wordsFuture');

  @override
  ObservableFuture<WordWrapper> get wordsFuture {
    _$wordsFutureAtom.reportRead();
    return super.wordsFuture;
  }

  @override
  set wordsFuture(ObservableFuture<WordWrapper> value) {
    _$wordsFutureAtom.reportWrite(value, super.wordsFuture, () {
      super.wordsFuture = value;
    });
  }

  final _$_WordStoreActionController = ActionController(name: '_WordStore');

  @override
  dynamic fetchWords() {
    final _$actionInfo =
        _$_WordStoreActionController.startAction(name: '_WordStore.fetchWords');
    try {
      return super.fetchWords();
    } finally {
      _$_WordStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPage(int i) {
    final _$actionInfo =
        _$_WordStoreActionController.startAction(name: '_WordStore.setPage');
    try {
      return super.setPage(i);
    } finally {
      _$_WordStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setType(WordType wordType) {
    final _$actionInfo =
        _$_WordStoreActionController.startAction(name: '_WordStore.setType');
    try {
      return super.setType(wordType);
    } finally {
      _$_WordStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void toggleFav({bool hide = false}) {
    final _$actionInfo =
        _$_WordStoreActionController.startAction(name: '_WordStore.toggleFav');
    try {
      return super.toggleFav(hide: hide);
    } finally {
      _$_WordStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void toggleHide() {
    final _$actionInfo =
        _$_WordStoreActionController.startAction(name: '_WordStore.toggleHide');
    try {
      return super.toggleHide();
    } finally {
      _$_WordStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void toggleLearned({dynamic hide = false}) {
    final _$actionInfo = _$_WordStoreActionController.startAction(
        name: '_WordStore.toggleLearned');
    try {
      return super.toggleLearned(hide: hide);
    } finally {
      _$_WordStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
currentPage: ${currentPage},
type: ${type},
wordsFuture: ${wordsFuture},
currentWord: ${currentWord},
showFinishPage: ${showFinishPage},
currentWords: ${currentWords}
    ''';
  }
}
