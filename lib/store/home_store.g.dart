// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeStore on _HomeStore, Store {
  Computed<int> _$allCountComputed;

  @override
  int get allCount => (_$allCountComputed ??=
          Computed<int>(() => super.allCount, name: '_HomeStore.allCount'))
      .value;
  Computed<int> _$learnedCountComputed;

  @override
  int get learnedCount =>
      (_$learnedCountComputed ??= Computed<int>(() => super.learnedCount,
              name: '_HomeStore.learnedCount'))
          .value;
  Computed<int> _$favCountComputed;

  @override
  int get favCount => (_$favCountComputed ??=
          Computed<int>(() => super.favCount, name: '_HomeStore.favCount'))
      .value;
  Computed<int> _$browseCountComputed;

  @override
  int get browseCount =>
      (_$browseCountComputed ??= Computed<int>(() => super.browseCount,
              name: '_HomeStore.browseCount'))
          .value;
  Computed<double> _$percentComputed;

  @override
  double get percent => (_$percentComputed ??=
          Computed<double>(() => super.percent, name: '_HomeStore.percent'))
      .value;

  final _$wordsFutureAtom = Atom(name: '_HomeStore.wordsFuture');

  @override
  ObservableFuture<WordWrapper> get wordsFuture {
    _$wordsFutureAtom.reportRead();
    return super.wordsFuture;
  }

  @override
  set wordsFuture(ObservableFuture<WordWrapper> value) {
    _$wordsFutureAtom.reportWrite(value, super.wordsFuture, () {
      super.wordsFuture = value;
    });
  }

  final _$_HomeStoreActionController = ActionController(name: '_HomeStore');

  @override
  dynamic fetchWords() {
    final _$actionInfo =
        _$_HomeStoreActionController.startAction(name: '_HomeStore.fetchWords');
    try {
      return super.fetchWords();
    } finally {
      _$_HomeStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
wordsFuture: ${wordsFuture},
allCount: ${allCount},
learnedCount: ${learnedCount},
favCount: ${favCount},
browseCount: ${browseCount},
percent: ${percent}
    ''';
  }
}
