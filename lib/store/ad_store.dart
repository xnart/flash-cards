import 'package:firebase_admob/firebase_admob.dart';
import 'package:mobx/mobx.dart';

part 'ad_store.g.dart';

class AdStore = _AdStore with _$AdStore;

abstract class _AdStore with Store {
  @observable
  InterstitialAd interstitialAd;

  @observable
  bool adReady = false;

  _AdStore() {
    initInterstitialAd()..load();
  }

  @action
  void showInterstitialAd() {
    if (adReady) {
      interstitialAd.show();
    }
  }

  @action
  InterstitialAd initInterstitialAd() {
    return interstitialAd = InterstitialAd(
      targetingInfo: MobileAdTargetingInfo(
          keywords: ["eğitim", "kelime öğren", "dil öğren", "yds", "yökdil"],
          testDevices: ["5A6CE483CB942DA7CD00FEC5D455ADC8", "70C6D2AEA9B37855E00AD5BC3A673D85"]),
      adUnitId: "ca-app-pub-3379426555442300/2213039723",
      // adUnitId: InterstitialAd.testAdUnitId,
      listener: (MobileAdEvent event) {
        switch (event) {
          case MobileAdEvent.loaded:
            adReady = true;
            break;
          case MobileAdEvent.failedToLoad:
            adReady = false;
            break;
          case MobileAdEvent.closed:
            interstitialAd = initInterstitialAd()..load();
            break;
          default:
            break;
        }
      },
    );
  }
}
