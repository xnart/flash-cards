import 'package:mobx/mobx.dart';

part 'loader_store.g.dart';

class LoaderStore = _LoaderStore with _$LoaderStore;

abstract class _LoaderStore with Store {
  @observable
  bool loading = false;
}
