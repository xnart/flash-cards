import 'word_model.dart';

class WordWrapper {
  List<WordModel> all = [];
  List<WordModel> favs = [];
  List<WordModel> hides = [];
  List<WordModel> learned = [];
  List<WordModel> browsable = [];

  WordWrapper({this.all, this.favs, this.hides, this.learned}) {
    browsable = all?.where((word) => !word.learned && !word.hide && !word.favorited)?.toList();
  }
}
