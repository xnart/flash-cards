import 'package:flash_cards/model/answer_model.dart';
import 'package:flash_cards/model/word_model.dart';

class QuestionModel {
  bool answered = false;
  WordModel word;
  List<AnswerModel> answers = [];
  AnswerModel selectedAnswer;

  QuestionModel({this.answered = false, this.word, this.answers, this.selectedAnswer});
}
