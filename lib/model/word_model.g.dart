// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'word_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WordModel _$WordModelFromJson(Map<String, dynamic> json) {
  return WordModel(
    id: json['id'] as String,
    word: json['word'] as String,
    meaning: json['meaning'] as String,
    example: json['example'] as String,
    picUrl: json['picUrl'] as String ?? 'https://picsum.photos/410/250',
    favorited: json['favorited'] as bool ?? false,
    learned: json['learned'] as bool ?? false,
    hide: json['hide'] as bool ?? false,
  );
}

Map<String, dynamic> _$WordModelToJson(WordModel instance) => <String, dynamic>{
      'id': instance.id,
      'word': instance.word,
      'meaning': instance.meaning,
      'example': instance.example,
      'picUrl': instance.picUrl,
      'favorited': instance.favorited,
      'learned': instance.learned,
      'hide': instance.hide,
    };
