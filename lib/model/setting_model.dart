import 'package:flash_cards/screen/quiz/quiz_type.dart';
import 'package:json_annotation/json_annotation.dart';

part 'setting_model.g.dart';

@JsonSerializable()
class SettingModel {
  @JsonKey(defaultValue: 10, nullable: true)
  int wordPerSession;

  @JsonKey(defaultValue: 10, nullable: true)
  int questionPerSession;

  @JsonKey(defaultValue: 6, nullable: true)
  int answerPerQuestion;

  @JsonKey(defaultValue: QuizType.MEANING)
  QuizType quizType;

  SettingModel(
      {this.wordPerSession = 10,
      this.questionPerSession = 10,
      this.answerPerQuestion = 6,
      this.quizType = QuizType.MEANING});

  factory SettingModel.fromJson(Map<String, dynamic> json) => _$SettingModelFromJson(json);

  Map<String, dynamic> toJson() => _$SettingModelToJson(this);
}
