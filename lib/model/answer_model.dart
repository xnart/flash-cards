import 'package:flash_cards/model/word_model.dart';
import 'package:flutter/material.dart';

class AnswerModel {
  bool correct = false;
  WordModel word;

  AnswerModel({this.correct = false, @required this.word});

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is AnswerModel && runtimeType == other.runtimeType && word.id == other.word.id;

  @override
  int get hashCode => word.id.hashCode;
}
