import 'package:json_annotation/json_annotation.dart';

part 'word_model.g.dart';

@JsonSerializable()
class WordModel {
  @JsonKey(ignore: true)
  String id;
  String word;
  String meaning;
  String example;
  String description;
  String wordType;
  @JsonKey(defaultValue: "https://picsum.photos/410/250")
  String picUrl;
  @JsonKey(defaultValue: false, ignore: true)
  bool favorited;
  @JsonKey(defaultValue: false, ignore: true)
  bool learned;
  @JsonKey(defaultValue: false, ignore: true)
  bool hide;

  WordModel(
      {this.id,
      this.word,
      this.meaning,
      this.example,
      this.picUrl,
      this.description,
      this.wordType,
      this.favorited = false,
      this.learned = false,
      this.hide = false});

  factory WordModel.fromJson(Map<String, dynamic> json) => _$WordModelFromJson(json);

  Map<String, dynamic> toJson() => _$WordModelToJson(this);

  @override
  bool operator ==(other) {
    return id == other.id;
  }

  @override
  int get hashCode => id.hashCode;
}
