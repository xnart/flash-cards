import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flash_cards/model/setting_model.dart';

import '../main.dart';

class SettingRepository {
  Future<SettingModel> fetchSettings() async {
    var snapshot = await firestore.collection("users").doc(FirebaseAuth.instance.currentUser.uid).get();
    if (snapshot.data() == null) {
      return SettingModel();
    }
    return SettingModel.fromJson(snapshot.data());
  }

  void saveSettings(SettingModel settingModel) {
    firestore
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser.uid)
        .set(settingModel.toJson(), SetOptions(merge: true));
  }
}
