import 'package:firebase_auth/firebase_auth.dart';
import 'package:flash_cards/model/word_model.dart';
import 'package:flash_cards/model/word_wrapper.dart';
import 'package:flash_cards/util/WordType.dart';

import '../main.dart';

class WordRepository {


  Future<WordWrapper> fetchWords() async {
    var dataSnapshot = await firestore.collection("words").get();
    List<String> favList = await fetchUserWordIds(WordType.FAV);
    List<String> hideList = await fetchUserWordIds(WordType.HIDE);
    List<String> learnedIds = await fetchUserWordIds(WordType.LEARNED);
    List<WordModel> recordModels = [];
    List<WordModel> favs = [];
    List<WordModel> learned = [];
    List<WordModel> hides = [];
    dataSnapshot.docs?.forEach((v) {
      var wordModel = WordModel.fromJson(v.data());
      wordModel.id = v.id;
      if (favList.contains(v.id)) {
        wordModel.favorited = true;
        favs.add(wordModel);
      }
      if (hideList.contains(v.id)) {
        wordModel.hide = true;
        hides.add(wordModel);
      }
      if (learnedIds.contains(v.id)) {
        wordModel.learned = true;
        learned.add(wordModel);
      }
      recordModels.add(wordModel);
    });
    return WordWrapper(all: recordModels, favs: favs, learned: learned, hides: hides);
  }

  Future<List<String>> fetchUserWordIds(WordType type) async {
    var dataSnapshot;
    if (type == WordType.FAV) {
      dataSnapshot =
          await firestore.collection("users").doc(FirebaseAuth.instance.currentUser.uid).collection("favs").get();
    } else if (type == WordType.LEARNED) {
      dataSnapshot =
          await firestore.collection("users").doc(FirebaseAuth.instance.currentUser.uid).collection("learned").get();
    } else if (type == WordType.HIDE) {
      dataSnapshot =
          await firestore.collection("users").doc(FirebaseAuth.instance.currentUser.uid).collection("hides").get();
    }
    List<String> ids = [];
    dataSnapshot.docs?.forEach((v) {
      ids.add(v.id);
    });
    return ids;
  }
}
