import 'package:flash_cards/repository/setting_repository.dart';
import 'package:flash_cards/repository/word_repository.dart';
import 'package:flash_cards/store/ad_store.dart';
import 'package:flash_cards/store/connectivity_store.dart';
import 'package:flash_cards/store/home_store.dart';
import 'package:flash_cards/store/setting_store.dart';
import 'package:flash_cards/store/word_store.dart';
import 'package:get/get.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(ConnectivityStore());
    Get.put(SettingRepository());
    Get.put(SettingStore());
    Get.put(WordRepository());
    Get.put(WordStore());
    Get.put(HomeStore());
    Get.put(AdStore());
  }
}
