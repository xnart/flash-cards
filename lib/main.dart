import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flash_cards/binding/HomeBinding.dart';
import 'package:flash_cards/i18n/app_translations.dart';
import 'package:flash_cards/screen/browse_screen.dart';
import 'package:flash_cards/screen/error.dart';
import 'package:flash_cards/screen/intro_screen.dart';
import 'package:flash_cards/screen/quiz/quiz_screen.dart';
import 'package:flash_cards/screen/settings.dart';
import 'package:flash_cards/store/quiz_store.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import 'screen/home.dart';

FirebaseApp app;
FirebaseAuth auth;
FirebaseFirestore firestore;
Connectivity connectivity;

Future<void> init() async {
  if (app.isNull) {
    app = await Firebase.initializeApp();
  }
  if (auth.isNull) {
    auth = FirebaseAuth.instanceFor(app: app);
  }

  if (auth.currentUser.isNull) {
    await auth.signInAnonymously();
  }

  if (firestore.isNull) {
    firestore = FirebaseFirestore.instanceFor(app: app);
    firestore.settings = Settings(persistenceEnabled: true, cacheSizeBytes: 40000000);
  }
  if (connectivity.isNull) {
    connectivity = Connectivity();
  }
  var status = await connectivity.checkConnectivity();
  if (status == ConnectivityResult.none) {
    firestore.disableNetwork();
  } else {
    firestore.enableNetwork();
  }
}

Future<String> determineRoute() async {
  var snapshot = await firestore.collection("users").doc(auth.currentUser.uid).get();
  var data = snapshot.data();
  if (data != null) {
    var showIntro = snapshot.data()["showIntro"] ?? true;
    if (showIntro) {
      return "/intro";
    }
  } else {
    return "/intro";
  }
  return "/";
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/UFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
  try {
    await init();
    String route = await determineRoute();
    runApp(MyApp(initialRoute: route));
  } catch (e) {
    print(e);
    runApp(MyApp(initialRoute: "/error"));
  }
}

class MyApp extends StatelessWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer = FirebaseAnalyticsObserver(analytics: analytics);
  final String initialRoute;

  MyApp({Key key, @required this.initialRoute}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      debugShowCheckedModeBanner: false,
      navigatorObservers: <NavigatorObserver>[MyApp.observer],
      theme: ThemeData(
        scaffoldBackgroundColor: Color(0xff373d5f),
        appBarTheme: AppBarTheme(color: Color(0xff272c49)),
        accentColor: Colors.pink,
        textTheme: GoogleFonts.ubuntuTextTheme(),
      ),
      locale: Locale("tr_TR"),
      supportedLocales: [Locale("tr"), Locale("en")],
      initialRoute: initialRoute,
      defaultTransition: Transition.downToUp,
      translations: AppTranslations(),
      getPages: [
        GetPage(name: "/", page: () => HomeScreen(), binding: HomeBinding()),
        GetPage(name: "/browse", page: () => BrowseScreen(), binding: HomeBinding()),
        GetPage(
            name: "/quiz",
            binding: BindingsBuilder(() => {Get.lazyPut<QuizStore>(() => QuizStore())}),
            page: () => QuizScreen(),
            transition: Transition.cupertinoDialog),
        GetPage(name: "/settings", page: () => SettingsScreen(), binding: HomeBinding()),
        GetPage(name: "/error", page: () => ErrorScreen()),
        GetPage(name: "/intro", page: () => IntroScreen()),
      ],
    );
  }
}
